Template.navbar.items = [
{
  url: '/',
  name: 'Home'
},
{
  url: '/livestream',
  name: 'Livestream'
},
{
  url: '/schedule',
  name: 'Schedule'
},
{
  url: '/speakers',
  name: 'Speakers'
},
{
  url: '/attendees',
  name: 'Attendees'
},
{
  url: '/about',
  name: 'About'
},
];

Template.navbar.events({
  'click .nav-button': function() {
    $('body').toggleClass('nav-visible');
  },

  'click header': function() {
    if ($('body').is('.fullscreen')) {
      $('body').removeClass('fullscreen');

      Meteor.map.setOptions({
        draggable: false,
        scrollwheel: false,
        zoomControl: false,
        mapTypeControl: false,
        scaleControl: false,
        navigationControl: false,
        overviewMapControl: false,
        disableDoubleClickZoom: true
      });
    }
  }
});
