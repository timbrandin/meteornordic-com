var map, markers = [];

Template.map_canvas.rendered = function() {
  map = initialize();
  Meteor.map = map;

  map.setOptions({
    draggable: false,
    scrollwheel: false,
    zoomControl: false,
    mapTypeControl: false,
    scaleControl: false,
    navigationControl: false,
    overviewMapControl: false,
    disableDoubleClickZoom: true
  });

  google.maps.event.addDomListener(window, 'resize', function() {
    map.setCenter(new google.maps.LatLng(58.4364939, -0.0791167));
  });
}

Meteor.startup(function() {
  Entities.find().observe({
    added: function(doc) {
      var ops = {
        coord: new google.maps.LatLng(doc.venue.lat, doc.venue.lon),
        url: '/meetup-pin.png',
        x: 13,
        y: 24,
        retina: true
      };

      if (map && map.place) {
        map.place(doc._id, ops);
      }
      else {
        markers.push({
          _id: doc._id,
          options: ops
        });
      }
    }
  });
});

Template.map_canvas.events({
  'click #map_canvas': function() {
    if (!$('body').is('.fullscreen')) {
      $('body').addClass('fullscreen');
      $('#map_canvas').height($(window).height() - 100);
      google.maps.event.trigger(map, 'resize');
      map.setCenter(new google.maps.LatLng(58.4364939, -0.0791167));

      map.setOptions({
        draggable: true,
        scrollwheel: true,
        zoomControl: false,
        mapTypeControl: false,
        navigationControl: false,
        overviewMapControl: true,
        disableDoubleClickZoom: false
      });
    }
  },
  'click .close': function() {
    $('body').removeClass('fullscreen');
    $('#map_canvas').height(0);
    google.maps.event.trigger(map, 'resize');
    map.setCenter(new google.maps.LatLng(58.4364939, -0.0791167));

    map.setOptions({
      draggable: false,
      scrollwheel: false,
      streetViewControl: false,
      mapTypeControl: false,
      navigationControl: false,
      overviewMapControl: false,
      disableDoubleClickZoom: true,
      panControl: false,
      zoomControl: false,
    });
  }
});

function initialize() {
  // Create an array of styles.
  var styles = [
    {
      "stylers": [
        { "color": "#131313" }
      ]
    },{
      "featureType": "landscape",
      "elementType": "geometry",
      "stylers": [
        { "color": "#7f7f7f" }
      ]
    },{
      "elementType": "labels",
      "stylers": [
        { "visibility": "off" }
      ]
    },{
      "featureType": "road",
      "elementType": "geometry",
      "stylers": [
        { "visibility": "off" }
      ]
    },{
      "featureType": "administrative",
      "elementType": "geometry",
      "stylers": [
        { "visibility": "off" }
      ]
    },{
      "featureType": "landscape.natural",
      "elementType": "geometry",
      "stylers": [
        { "visibility": "on" }
      ]
    }
  ];

  // Create a new StyledMapType object, passing it the array of styles,
  // as well as the name to be displayed on the map type control.
  var styledMap = new google.maps.StyledMapType(styles,
    {name: "Meteor Nordic"});

  var w = $(window).width();

  var mapOptions = {
    zoom: (w < 600) ? 3 : 4,
    center: new google.maps.LatLng(58.4364939, -0.0791167),
    optimized: true,
    draggable: false,
    scrollwheel: false,
    streetViewControl: false,
    mapTypeControl: false,
    navigationControl: false,
    overviewMapControl: false,
    disableDoubleClickZoom: true,
    panControl: false,
    zoomControl: false,
    mapTypeId: google.maps.MapTypeId.TERRAIN,
    mapTypeControlOptions: {
      mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
    }
  };

  var map = new google.maps.Map(document.getElementById('map_canvas'),
      mapOptions);

  //Associate the styled map with the MapTypeId and set it to display.
  map.mapTypes.set('map_style', styledMap);
  map.setMapTypeId('map_style');

  Session.set('rendered', true);

  // Replace markers if they have been added before.
  var _markers = _.clone(markers);
  markers = [];
  _.each(_markers, function(marker) {
    placeMarker(marker._id, marker.options);
  });

  function placeMarker(_id, options) {
    // Wait for image to load.
    var img = $('<img>', {src: options.url}).load(function() {
      // Define goal marker image.
      var image = {
        url: options.url,
        // This marker is 50 pixels wide by 25 pixels tall.
        size: new google.maps.Size(this.width, this.height),
        // The origin for this image is 0,0.
        origin: new google.maps.Point(0,0),
        // The anchor for this image is the base of the flagpole at 0,32.
        anchor: new google.maps.Point(options.x, options.y)
      };

      if (options.retina) {
        image.scaledSize = new google.maps.Size(this.width / 2, this.height / 2);
      }

      // Draw the goal on the map.
      var marker = new google.maps.Marker({
        position: options.coord,
        map: map,
        icon: image,
        zIndex: options.zIndex || 0
      });

      // Add marker to the list of all markers.
      markers.push({
        _id: _id,
        options: options,
        marker: marker
      });
    });
  }

  // Helper to clear away everything on the map.
  function clearMap() {
    // Clear all markers.
    for (var i = 0; i < markers.length; i++) {
      markers[i].marker.setMap(null);
    }
    markers = [];
  }

  // Helper to remove a marker from the map and list of markers.
  function removeMarker(_id) {
    // Find the marker.
    var marker = _.findWhere(markers, {_id: _id});
    if (marker) {
      // Remove the marker from the map.
      marker.marker.setMap(null);
      // Remove the marker from the list of markers.
      markers = _.without(markers, marker);
    }
    marker
  }

  return _.extend(map, {
    place: function(_id, options) {
      placeMarker(_id, options);
    },

    remove: function(_id) {
      removeMarker(_id);
    },

    clear: function() {
      clearMap();
    }
  });
}
