Template.layout.rendered = function() {
  var $header = $('header');
  var $window = $(window);
  var $body = $('body');
  var $mobileNavRegion = $('.mobile-nav .region');
  var $navUl = $('.mobile-nav ul');
  var $headerLogo = $('header .logo');
  var $headerLogoImage = $('header .logo img');
  var $navButton = $('.nav-button');

  if(is_touch_device()) {
    $('body').addClass('touch');
  }
  else {
    $('body').addClass('no-touch');
  }

  $(window).resize(function() {
    var mobile = 970;
    var width = $window.width();

    $body.toggleClass('mobile', width < mobile);
    $body.toggleClass('desktop', width >= mobile);
    var navHeight = $navUl.height();
    $mobileNavRegion.height(navHeight);

    Session.set('width', width);
  }).trigger('resize');

  $(window).scroll(function() {
    // Transition goes from 1 to 0 from top of window to 300px down.
    var transition = createTransition($window.scrollTop(), 0, 300);

    // Transition opacity.
    var opacity = scaleTransition(transition, 0.1, 0.9);

    // Transition color.
    var r = Math.round((1 - transition) * 27);
    var g = Math.round((1 - transition) * 78);
    var b = Math.round((1 - transition) * 114);
    var color = [r,g,b].join(',');

    $header.css({
      'background-color': 'rgba(' + color + ',' + opacity + ')'
    });
  }).trigger('scroll');

  // Helper to create transitions for any value x.
  function createTransition(x, min, max) {
    return Math.min(max, Math.max(min, max - x)) / max;
  }

  // Helper to scale a transition.
  function scaleTransition(transition, start, stop) {
    var scaledTransition = transition * (start - stop);
    return scaledTransition + stop;
  }

  $('.nav-button').bind('touchstart', function(e) {
    e.preventDefault();
    $(this).trigger('click');
  });

  var touchStarted = false;
  $('li a').bind('touchstart', function(e) {
    touchStarted = true;
  });

  $('li a').bind('touchmove', function(e) {
    touchStarted = false;
  });

  $('li a').bind('touchend', function(e) {
    if (touchStarted && !$(this).parent('li').hasClass('active')) {
      $(this).trigger('click');
      $('body').removeClass('ready');
      setTimeout(function() {
        Router.go($(this).attr('href'));
        touchStarted = false;
      }, 10);
    }
  });

  $('li a').bind('click', function(e) {
    e.preventDefault()
    Router.go(e.currentTarget.href);
    return;
  });
}

function is_touch_device() {
  // Check if device has touch support

  return !!('ontouchstart' in window) // works on most browsers
  || !!(window.navigator.msMaxTouchPoints); // works on ie10
};
