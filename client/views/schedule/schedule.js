Template.schedule.helpers({
  attributes: function() {
    var start = moment.tz(this.start, 'Europe/Stockholm');
    var end = moment.tz(this.end, 'Europe/Stockholm');
    var now = moment(Session.get('now'));

    if (now.isAfter(end)) {
      return {class: 'item passed'};
    }
    else if ((now.isAfter(start) || now.isSame(start)) && now.isBefore(end)) {
      return {class: 'item now'};
    }
  },

  now: function() {
    var start = moment.tz(this.start, 'Europe/Stockholm');
    var end = moment.tz(this.end, 'Europe/Stockholm');
    var now = moment(Session.get('now'));

    if ((now.isAfter(start) || now.isSame(start)) && now.isBefore(end)) {
      var self = this;
      setTimeout(function() {
        var item = $('#' + self._id);
        if (item) {
          $("html, body").animate({scrollTop: item.position().top - $(window).height() * 0.2}, 1500);
        }
      }, 300);
      return 'NOW';
    }
    if (/pm/i.test(moment().toString())) {
      return start.format('hh:mm');
    }
    else {
      var local = new Date();
      var start = moment.tz(this.start, 'Europe/Stockholm');
      var zone = start.zone();
      var localStart = start.subtract(moment().zone(), 'minutes').add(zone, 'minutes');

      return localStart.format('HH:mm');
    }
  },

  groups: function() {
    return Entities.find({}, {sort: {'group.members': -1}});
  },

  members: function() {
    return Regs.find({_meetup: this._id}, {sort: {created: 1}});
  }
});
