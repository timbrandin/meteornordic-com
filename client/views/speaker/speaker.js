Template.speaker.events({
  'keyup textarea': function(e, t) {
    var value = t.find('textarea').value;
    News.update({_id: this._id}, {$set: {body: value}});
  },

  'keyup input': function(e, t) {
    var value = t.find('input').value;
    News.update({_id: this._id}, {$set: {summary: value}});
  }
});

Template.speaker.helpers({
  time: function() {
    var localTime = Session.get('now');
    if (/pm/i.test(moment().toString())) {
      return moment(localTime).tz(this.timezone).format('h:mm:ss a');
    }
    else {
      return moment(localTime).tz(this.timezone).format('HH:mm:ss');
    }
  }
})
