Template.attendees.helpers({
  regs: function() {
    return Regs.find({_meetup: this._id}, {sort: {created: 1}});
  }
});

Template.attendees.rendered = function() {
  var route = Router.current();

  setTimeout(function() {
    var table = $('#' + route.params.hash);
    if (route.params.hash) {
      $('body').animate({
        scrollTop: table.position().top - 150 - 100
      });
    }
  }, 300);
}
