Template.front.rendered = function() {
  $('.articles img').unbind('load.articles');
  $('.articles img').bind('load.articles', function() {
    // Equal height.
    setEqualHeight('.articles .article');
  });

  $(window).unbind('resize.articles');
  $(window).bind('resize.articles', function() {
    // Equal height.
    setEqualHeight('.articles .article');
  }).trigger('resize');
}

function setEqualHeight(selector) {
  var maxHeight = 0;

  // Reset heights.
  $(selector).removeAttr('style');

  _.each($(selector), function(o) {
    maxHeight = Math.max(maxHeight, $(o).height() + $(o).next('.call-to-action').height());
  });

  $(selector).css('min-height', maxHeight);
}
