MyEvents = new Meteor.Collection(null);

Meteor.startup(function() {
  Meteor.users.find().observe({
    added: function() {
      Meteor.call('getEvents', function(err, res) {
        if (!err) {
          var date = moment.tz('2014-10-15T19:00', 'Europe/Stockholm');
          _.each(res, function(doc) {
            var eventDate = moment.tz(doc.time, 'UTC').tz('Europe/Stockholm');
            if (date.startOf('day').isSame(eventDate.startOf('day'))) {
              MyEvents.upsert({_id: doc.id}, {$set: doc});
            }
          });
        }
      });
    },
    removed: function() {
      _.each(MyEvents.find().fetch(), function(doc) {
        MyEvents.remove({_id: doc._id});
      });
    }
  })
});

Template.about.helpers({
  myEvents: function() {
    return MyEvents.find();
  },

  selected: function() {
    var found = Entities.findOne({_id: this._id});
    return found ? {class: 'selected add-event', disabled: true, title: 'This event has already been added.'} : {class: 'add-event'};
  }
});

Template.about.events({
  'click .add-event': function() {
    var doc = this;
    Entities.insert(doc);
  }
});
