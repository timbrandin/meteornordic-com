// Get the session variable for the active path.
activePath = function(){
  return Session.get('activePath');
}

// Determine if current link should be active.
UI.registerHelper('active', function(path) {
  return Router.current().route.originalPath == path || activePath() == path ? {class: 'active'} : '';
});

UI.registerHelper('hideMobile', function() {
  var w = Session.get('width');
  return w < 780 || (w > 1100 && w < 1600);
});

UI.registerHelper('hideThin', function() {
  var w = Session.get('width');
  return w < 600 || (w > 1100 && w < 1280);
});

UI.registerHelper('width', function(percent) {
  return {
    style: 'width:' + Session.get('width') * (parseInt(percent)/100) + 'px'
  };
});

/** Converts numeric degrees to radians */
if (typeof(Number.prototype.toRad) === "undefined") {
  Number.prototype.toRad = function() {
    return this * Math.PI / 180;
  }
}

/** Converts numeric degrees to radians */
if (typeof(Number.prototype.toDeg) === "undefined") {
  Number.prototype.toDeg = function() {
    return this * 180 / Math.PI;
  }
}

/** Rounds numbers to a certain level of detail. */
if (typeof(Number.prototype.round) === "undefined") {
  Number.prototype.round = function(decimals) {
    return Math.round(this * decimals * 10) / (decimals * 10);
  }
}

UI.registerHelper('countdown', function(datetime) {
  var now = moment(Session.get('now'));
  var diff = moment.tz(datetime, 'Europe/Stockholm').diff(now) / 1000;
  var days = Math.max(0, Math.floor(diff/3600/24));
  var hours = Math.max(0, Math.floor(diff/3600 % 24));
  var minutes = Math.max(0, Math.floor(diff/60 % 60));
  var seconds = Math.max(0, Math.floor(diff % 60));
  var date_parts = {};

  var ended = true;
  if (days > 0) {
    date_parts.days = days;
    ended = false;
  }
  if (hours >= 0) {
    date_parts.hours = hours;
    ended = false;
  }
  if (minutes >= 0) {
    date_parts.minutes = minutes;
    ended = false;
  }
  if (seconds >= 0) {
    date_parts.seconds = seconds;
    ended = false;
  }

  if (days == 0 && hours == 0 && minutes < 30) {
    if (minutes == 0 && seconds == 0) {
      date_parts.hasStarted = true;
      setTimeout(function() {
        $(window).trigger('resize');
      }, 0);
    }
    else if ((minutes > 0) || (minutes == 0 && seconds > 0)) {
      date_parts.isAboutToStart = true;
      setTimeout(function() {
        $(window).trigger('resize');
      }, 0);
    }
    else {
      setTimeout(function() {
        $(window).trigger('resize');
      }, 0);
    }
  }

  if (!ended) {
    return date_parts;
  }
});

UI.registerHelper('xx', function(n) {
  if (n >= 0) {
    return n < 10 ? '0' + n : n;
  }
});

UI.registerHelper('formatDate', function(date, format) {
  return moment.tz(date, 'Europe/Stockholm').format(format);
});

UI.registerHelper('calendarDateTz', function(date, timezone) {
  if (date) {
    return moment(date).fromNow();
  }
});

Meteor.startup(function() {
  Meteor.setInterval(function() {
    Session.set('now', (+new Date));
  }, 990);
});
