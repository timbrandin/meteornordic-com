var subs = new SubsManager();

Router.configure({
  layoutTemplate: 'layout',
  onBeforeAction: function() {
    $('body').removeClass('ready nav-visible');
  },
  onAfterAction: function() {
    $('body').scrollTop(0);
    setTimeout(function() {
      $('body').addClass('ready');
    }, 150);
  }
});

Router.map(function() {

  this.route('home', {
    path: '/',
    template: 'divided',
    waitOn: function() {
      subs.subscribe('news');
      return subs.subscribe('regscount');
    },
    onBeforeAction: function() {
      $('body').addClass('reverse front');

      document.title = 'Meteor Nordic Meetup | October 15';
      setMetaDescription('Get to know the Nordic Meteor community, meetups in the Nordic region get together for a Google Hangout on Air.');
    },
    action: function() {
      this.render();
      this.render('page', {to: 'left'});
      this.render('front', {to: 'page'});
      this.render('front-banner', {to: 'right'});
    },
    data: function() {
      return {
        image: '/top-banners-48.jpg',
        viewers: Counts.get('activesubs'),
        count: Counts.get('regs'),
        speakers: News.find({}, {sort: {created: 1}})
      }
    },
    onStop: function() {
      $('body').removeClass('reverse front');
    }
  });

  this.route('about', {
    path: '/about',
    template: 'divided',
    waitOn: function() {
      subs.subscribe('regscount')
      return subs.subscribe('entities');
    },
    onBeforeAction: function() {
      document.title = 'Meteor Nordic Meetup | About';
      setMetaDescription('Join the Meteor community in October for Meteor Nordic Meetup on Wednesday October 15.');
    },
    action: function() {
      this.render();
      this.render('page', {to: 'left'});
      this.render('about', {to: 'page'});
      this.render('map_canvas', {to: 'right'});
    },
    data: function() {
      return {
        entities: Entities.find({}, {sort: {yes_rsvp_count: -1}}),
        count: Counts.get('regs')
      }
    },
    onStop: function() {
      Session.set('selectedDistance', null);
      Session.set('rendered', false);
      $('body').removeClass('fullscreen');
    }
  });

  this.route('schedule', {
    path: '/schedule',
    template: 'divided',
    waitOn: function() {
      subs.subscribe('entities');
      subs.subscribe('regs');
      return subs.subscribe('schedule');
    },
    onBeforeAction: function() {
      $('body').addClass('no-map-mobile');
      document.title = 'Meteor Nordic Meetup | Schedule';
      setMetaDescription('The battleplan for the live event. Broadcast starts Wednesday October 15 2014, this content may change.');
    },
    action: function() {
      this.render();
      this.render('page', {to: 'left'});
      this.render('schedule', {to: 'page'});
      this.render('map_canvas', {to: 'right'});
    },
    data: function() {
      return {
        items: Schedule.find({}, {sort: {created: 1}}),
        speakers: News.find()
      }
    },
    onStop: function() {
      Session.set('selectedDistance', null);
      Session.set('rendered', false);
      $('body').removeClass('no-map-mobile schedule');
    }
  });

  this.route('livestream', {
    path: '/livestream',
    template: 'iframe',
    onBeforeAction: function() {
      $('body').addClass('reverse frame');
      $(window).trigger('resize');

      document.title = 'Meteor Nordic Meetup | Livestream';
      setMetaDescription('Watch the live stream online, this is where you can watch it other than on Google Plus or Youtube.');
    },
    action: function() {
      this.render();
    },
    data: function() {
      return {
        url: '//www.youtube.com/embed/h_Zt6-o-qEQ?autoplay=1&showinfo=0&controls=0'
      }
    },
    onStop: function() {
      $('body').removeClass('reverse frame');
    }
  });

  this.route('attendees', {
    path: '/attendees',
    template: 'divided',
    waitOn: function() {
      subs.subscribe('entities');
      return subs.subscribe('regs');
    },
    onBeforeAction: function() {
      $('body').addClass('reverse wide');
      document.title = 'Meteor Nordic Meetup | Attendees';
      setMetaDescription('This list of attendies updates once every hour from Meetup.com. Göteborg, Kristiansand, Stavanger and Stockholm.');
    },
    action: function() {
      this.render();
      this.render('page', {to: 'left'});
      this.render('attendees', {to: 'page'});
      this.render('top-banner', {to: 'right'});
    },
    data: function() {
      return {
        title: 'Attendees',
        image: '/top-banners-48.jpg',
        meetups: Entities.find({}, {sort: {yes_rsvp_count: -1}})
      }
    },
    onStop: function() {
      $('body').removeClass('reverse wide');
    }
  });

  this.route('speakers', {
    path: '/speakers',
    template: 'divided',
    waitOn: function() {
      return subs.subscribe('news');
    },
    onBeforeAction: function() {
      $('body').addClass('reverse wide');
      document.title = 'Meteor Nordic Meetup | Speakers';
      setMetaDescription('Who are the speakers? Josh, Carl, Ekate, Auronoda and Robert.');
    },
    action: function() {
      this.render();
      this.render('page', {to: 'left'});
      this.render('speakers', {to: 'page'});
      this.render('top-banner', {to: 'right'});
    },
    data: function() {
      return {
        title: 'Speakers',
        image: '/top-banners-48.jpg',
        speakers: News.find({}, {sort: {created: 1}})
      }
    },
    onStop: function() {
      $('body').removeClass('reverse wide');
    }
  });

  this.route('speaker', {
    path: '/speakers/:url_name',
    template: 'divided',
    waitOn: function() {
      return subs.subscribe('news');
    },
    onBeforeAction: function() {
      $('body').addClass('reverse wide');
      document.title = 'Meteor Nordic Meetup | Speakers';
    },
    action: function() {
      this.render();
      this.render('page', {to: 'left'});
      this.render('speaker', {to: 'page'});
      this.render('top-banner', {to: 'right'});
    },
    data: function() {
      var speaker = News.findOne({url_name: this.params.url_name});

      if (speaker) {
        document.title = 'Meteor Nordic Meetup | Speaker ' + speaker.full_name;
        setMetaDescription(speaker.summary.slice(0, 150) + ' ...');

        return {
          title: speaker.name,
          company: speaker.company,
          company_url: speaker.company_url,
          image: speaker.background,
          article: speaker
        }
      }
    },
    onStop: function() {
      $('body').removeClass('reverse wide');
    }
  });
});

// Helper to set the meta description for a page.
function setMetaDescription(content) {
  $('meta[name="description"]').attr('content', content);
}
