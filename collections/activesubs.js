ActiveSubs = new Mongo.Collection('activesubs');
TotalVisitors = new Meteor.Collection('totalvisitors');

if (Meteor.isServer) {
  var geoip = Meteor.npmRequire('geoip-lite');

  Meteor.startup(function() {
    ActiveSubs.remove({});
  });

  Meteor.publish('activesubcount', function(details) {
    var sub = this;

    var ip = this.connection.clientAddress;
    var geo = geoip.lookup(ip);

    // Extend passed subscription details.
    var activeUserSub = _.extend({
      _sub: sub._subscriptionId,
      _owner: sub.userId || null,
      geo: geo || {city: 'Göteborg', country: 'SE'}
    }, details);

    ActiveSubs.upsert({_id: activeUserSub._id}, activeUserSub);

    // Publish total count of registrations.
    Counts.publish(this, 'activesubs', ActiveSubs.find());

    sub.onStop(function() {
      ActiveSubs.remove({_sub: activeUserSub._sub});
    });

    return TotalVisitors.find();
  });

  Meteor.publish('activesubs', function(details) {
    return ActiveSubs.find();
  });

  ActiveSubs.find().observe({
    added: function() {
      TotalVisitors.upsert({_id: 'total'}, {$inc: {count: 1}});
    }
  });
}
else {
  var names = ['Novato', 'Bolide', 'Marília', 'Murnpeowie', 'Esquel', 'Benld', 'Allende', 'Babubirito', 'Gibeon', 'Hoba', 'Kaidun', 'Murchison', 'Nōgata', 'Orgueil'];
  var name = names[Math.floor(Math.random() * (names.length-1))].replace(/\d+/, '').trim();
  var uuid = Meteor.uuid();
  Deps.autorun(function() {
    var route = Router.current();
    var title = document.title.replace(/Meteor Nordic Meetup \| /, '').replace('18 oktober 2014', 'Framsidan');
    Meteor.subscribe('activesubcount', {
      _id: uuid,
      where: title,
      shoe: name,
      movedAt: Date.now()
    });
  });
}
