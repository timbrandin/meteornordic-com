if (Meteor.isClient) {
  Accounts.ui.config({
    requestPermissions: {
      meetup: ['ageless', 'basic']
    }
  });
}
