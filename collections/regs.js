Regs = new Mongo.Collection('regs');

if (Meteor.isServer) {
  var Cheerio = Meteor.npmRequire('cheerio');
  var tidy = Meteor.wrapAsync(Meteor.npmRequire('htmltidy').tidy);

  Meteor.startup(function() {
    // Set a cron task to recurringly sync.
    createSyncAttendeesTask();
    SyncedCron.start();

    Entities.find().observe({
      added: function(doc) {
        getEventAttendees(doc);
      }
    })
  });

  Meteor.publish('regs', function() {
    // Publish total count of registrations.
    Counts.publish(this, 'regs', Regs.find(), {noReady: true});

    return Regs.find({}, {fields: {seenAt: false}});
  });

  Meteor.publish('regscount', function() {
    // Publish total count of registrations.
    Counts.publish(this, 'regs', Regs.find());
  });

  /**
   * Helper to create a synced cron task for each project added.
   */
  function createSyncAttendeesTask() {
    SyncedCron.add({
      name: 'Sync RSVPs for each event on Meetup',
      schedule: function(parser) {
        return parser.text('every 10 minutes');
      },
      job: function() {
        var before = Date.now();
        getAllAttendees();
        cleanRegistered(before);
      }
    });
  }

  function getAllAttendees() {
    var entities = Entities.find().fetch();
    _.each(entities, function(doc) {
      getEventAttendees(doc);
    });
  }

  function getEventAttendees(doc) {
    var user = Meteor.users.findOne({
      _id: doc._user,
      'services.meetup': {$exists: 1}
    });

    var rsvps = getEventRSVPs(doc, user);
    if (rsvps && rsvps.results) {
      _.each(rsvps.results, function(rsvp) {
        Regs.upsert({_id: rsvp.rsvp_id}, {$set: _.extend(rsvp, {
          seenAt: Date.now(),
          _meetup: doc._id
        })});
      });
    }
  }

  function getEventRSVPs(doc, user) {
    if (Meetup && user) {
      return Meetup.get('2/rsvps', {
        event_id: doc.id,
        rsvp: 'yes'
      }, {user: user});
    }
  }

  function cleanRegistered(timestamp) {
    var registered = Regs.find({$or: [{seenAt: {$exists: false}}, {seenAt: {$lt: timestamp}}]}).fetch();

    _.each(registered, function(registree) {
      Regs.remove({_id: registree._id});
    });

    return registered.length;
  }
}
