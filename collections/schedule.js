Schedule = new Meteor.Collection('schedule');

if (Meteor.isServer) {
  Meteor.publish('schedule', function() {
    var schedule = Schedule.find();

    var confirmed = [
      'Josh-Owens',
      'Ekaterina-Kuznetsova',
      'Arunoda-Susiripala',
      'Carl-Littke',
      'Robert-Dickert',
    ];

    var speakers = News.find({url_name: {$in: confirmed}});
    return [schedule, speakers];
  });

  Meteor.startup(function() {

    // Remove all for testing.
    Schedule.remove({});

    if (Schedule.find().count() == 0) {
      var schedule_items = [
        {
          title: 'Groups present themselves',
          summary: 'Presentation of all local Meetup groups.',
          start: moment.tz('2014-10-15T19:00', 'Europe/Stockholm').valueOf(),
          end: moment.tz('2014-10-15T19:15', 'Europe/Stockholm').valueOf(),
          group_presentation: true
        },
        {
          title: 'How to write CPU Optimized Meteor Apps',
          summary: 'Arunoda Susiripala will show why some Meteor apps have such high CPU usage, and then show us some tricks how to get around those hassles.',
          start: moment.tz('2014-10-15T19:15', 'Europe/Stockholm').valueOf(),
          end: moment.tz('2014-10-15T19:45', 'Europe/Stockholm').valueOf(),
          people: [
            {
              name: 'Arunoda Susiripala',
              image: '/speaker-arunoda.png',
            },
          ]
        },
        {
          title: 'Short break',
          summary: 'Time for our experts to tune in on the stream and Hangout.',
          start: moment.tz('2014-10-15T19:45', 'Europe/Stockholm').valueOf(),
          end: moment.tz('2014-10-15T20:00', 'Europe/Stockholm').valueOf()
        },
        {
          title: 'Hotpanel discussion',
          summary: 'Robert Dickert - community engineer at MDG in San Fransisco will help lead a panel with experts from around the community to discuss what\'s great with Meteor and mobile applications, and answer questions coming in through the Q&A app in Google Hangout.',
          start: moment.tz('2014-10-15T20:00', 'Europe/Stockholm').valueOf(),
          end: moment.tz('2014-10-15T21:00', 'Europe/Stockholm').valueOf(),
          people: [
            {
              name: 'Ekaterina Kuznetsova',
              image: '/speaker-ekate.png',
            },
            {
              name: 'Josh Owens',
              image: '/speaker-joshowens.png',
            },
            {
              name: 'Arunoda Susiripala',
              image: '/speaker-arunoda.png',
            },
            {
              name: 'Carl Littke',
              image: '/speaker-carllittke.png',
            },
            {
              name: 'Robert Dickert',
              image: '/robertdickert.png',
            }
          ]
        },
        {
          title: 'Thanks for watching',
          summary: 'The organizers would like to send out an extra thanks to all of you who watched and attended a Meteor Nordic Meetup.',
          start: moment.tz('2014-10-15T21:00', 'Europe/Stockholm').valueOf(),
          end: moment.tz('2014-10-15T21:15', 'Europe/Stockholm').valueOf(),
          people: [
            {
              name: 'Tim Brandin',
              image: '/timbrandin.png'
            },
            {
              name: 'Razvan Teslaru',
              image: '/razvanteslaru.png'
            },
            {
              name: 'Robert Dickert',
              image: '/robertdickert.png'
            },
            {
              name: 'Kevin Kaland',
              image: '/kevinkaland.png'
            },
            {
              name: 'Tobias Berg',
              image: '/tobiasberg.png'
            }
          ]
        },
      ];

      _.each(schedule_items, function(document){
        Schedule.insert(document);
      });
    }
  });
}
