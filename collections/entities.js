Entity = function(document) {
  _.extend(this, document);
};

Entity.prototype = {
  constructor: Entity,

  country: function() {
    var iso_countries = ['se', 'no', 'dk', 'fi', 'us', 'is'];
    var countries = ['Sweden', 'Norway', 'Denmark', 'Finland', 'USA', 'Iceland'];
    var i = _.indexOf(iso_countries, this.venue.country);
    return countries[i];
  },

  formatTime: function() {
    return moment.tz(this.time, this.group.timezone).format('HH:mm');
  },

  remaining: function() {
    return this.rsvp_limit - this.yes_rsvp_count;
  }
};

Entities = new Meteor.Collection('entities', {
  transform: function(document) {
    return new Entity(document);
  }
});

if (Meteor.isServer) {
  Meteor.publish('entities', function() {
    return Entities.find();
  });

  Entities.allow({
    insert: function(userId, doc) {
      return userId != null;
    }
  });

  Meteor.startup(function() {
    // Set a cron task to recurringly sync.
    createSyncTask();

    Entities.find().observe({
      added: function(doc) {
        updateEvent(doc);
      }
    });
  });

  function createSyncTask() {
    SyncedCron.add({
      name: 'Sync events and groups from Meetup',
      schedule: function(parser) {
        return parser.text('every 10 minutes');
      },
      job: function() {
        updateGroups();
      }
    });
  }

  function updateGroups() {
    var entities = Entities.find().fetch();
    _.each(entities, function(doc) {
      updateEvent(doc);
    });
  }

  function updateEvent(doc) {
    var user = Meteor.users.findOne({
      _id: doc._user,
      'services.meetup': {$exists: 1}
    });

    var eventDetails = getEventDetails(doc, user);
    if (eventDetails) {
      Entities.update({_id: doc._id}, {$set: _.omit(eventDetails, 'group')});
    }

    var groupDetails = getGroupDetailsForEvent(doc, user);
    if (groupDetails && groupDetails.results) {
      Entities.update({_id: doc._id}, {$set: {group: groupDetails.results[0]}});
    }
  }

  function getEventDetails(doc, user) {
    if (Meetup && user) {
      return Meetup.get('2/event/' + doc.id, {}, {user: user});
    }
  }

  function getGroupDetailsForEvent(doc, user) {
    if (Meetup && user) {
      var event = Meetup.get('2/groups', {
        member_id: 'self',
        group_id: doc.group.id
      }, {user: user});

      return event;
    }
  }

  Meteor.methods({
    getEvents: function() {
      if (this.userId) {
        var user = Meteor.users.findOne({
          _id: this.userId,
          'services.meetup': {$exists: 1}
        });

        if (Meetup && user) {
          var eventsData = Meetup.get('2/events', {
            member_id: 'self',
            status: 'upcoming',
            rsvp: 'yes'
          }, {user: user});

          var events = [];
          _.each(eventsData.results, function(doc) {
            events.push(_.extend(doc, {_user: user._id}));
          });

          return events;
        }
      }
    }
  });
}
