News = new Meteor.Collection('news');

if (Meteor.isServer) {

  News.allow({
    insert: function(userId) {
      var user = Meteor.users.findOne({
        _id: userId,
        'services.meetup': {$exists: 1}
      });

      return user &&
        user.services &&
        user.services.meetup &&
        user.services.meetup.id == 87389252;
    },

    update: function(userId) {
      var user = Meteor.users.findOne({
        _id: userId,
        'services.meetup': {$exists: 1}
      });

      return user &&
        user.services &&
        user.services.meetup &&
        user.services.meetup.id == 87389252;
    }
  });

  Meteor.startup(function() {

    // Remove all for testing.
    // News.remove({});

    if (News) { //}.find().count() == 0) {
      var news_documents = [
        {
          name: 'Ekate',
          full_name: 'Ekaterina Kuznetsova',
          url_name: 'Ekaterina-Kuznetsova',
          role: 'Software System Engineer',
          city: 'San Fransisco',
          country: 'USA',
          url_country: 'america',
          timezone: 'America/Los_Angeles',
          company: 'Meteor Developement Group',
          company_short: 'MDG',
          company_url: 'http://meteor.com',
          image: '/speaker-ekate.png',
          twitter_handle: '0ekate',
          youtube: 'nOdLlvmJgRk',
          summary: 'Ekate is the core developer at MDG most known from her presentation of the new package system for Meteor. She is best recognized by her super cool hairstyle and brilliant smile.',
          background: '/bg-meteor.png',
          created: (+new Date)
        },
        {
          name: 'Josh',
          full_name: 'Josh Owens',
          url_name: 'Josh-Owens',
          role: 'Chief Cat Wrangler (Partner)',
          city: 'Cincinnati',
          country: 'USA',
          url_country: 'america',
          timezone: 'America/New_York',
          company: 'Meteor Club',
          company_short: 'Meteor Club',
          company_url: 'http://meteorjs.club/',
          image: '/speaker-joshowens.png',
          twitter_handle: 'joshowens',
          youtube: 'JOpPG6kiud4',
          summary: 'Josh Owens is most known from The Meteor Podcast, and for running Meteor Club and the Meteor news site Crater. Josh is also one of the guys behind the testing framework Velocity.',
          background: '/bg-meteorclub.png',
          created: (+new Date)
        },
        {
          name: 'Arunoda',
          full_name: 'Arunoda Susiripala',
          url_name: 'Arunoda-Susiripala',
          role: 'Founder of Kadira and Meteor hacker',
          city: 'Yalu',
          country: 'Sri Lanka',
          url_country: 'sri_lanka',
          timezone: 'Asia/Colombo',
          company: 'Meteor Hacks',
          company_short: 'Meteor Hacks',
          company_url: 'http://meteorhacks.com',
          image: '/speaker-arunoda.png',
          twitter_handle: 'arunoda',
          youtube: 'dUBBnBOW7uc',
          summary: 'Arunoda is the founder of Kadira and an often great inspiration for anything hard-core Meteor or Node.js. He\'s also the author of the well known blog and newsletter at Meteor Hacks.',
          background: '/bg-meteorhacks.png',
          created: (+new Date)
        },
        {
          name: 'Carl',
          full_name: 'Carl Littke',
          url_name: 'Carl-Littke',
          role: 'Co-founder and developer',
          city: 'Stockholm',
          country: 'Sweden',
          url_country: 'sweden',
          timezone: 'Europe/Stockholm',
          company: 'Lookback',
          company_short: 'Lookback',
          company_url: 'http://lookback.io',
          image: '/speaker-carllittke.png',
          twitter_handle: 'CarlLittke',
          youtube: '2p4bWcmcvww',
          summary: 'Carl is the co-founder and developer of Lookback, a tool for discovering how people really uses your app. Lookback makes it a breeze to record bugs and user experience.',
          background: '/bg-lookback.png',
          created: (+new Date)
        },
        {
          name: 'Robert',
          full_name: 'Robert Dickert',
          url_name: 'Robert-Dickert',
          role: 'Community Engineer',
          city: 'Lafayette',
          country: 'Colorado',
          url_country: 'america',
          timezone: 'America/Denver',
          company: 'Meteor Developement Group',
          company_short: 'MDG',
          company_url: 'http://meteor.com',
          image: '/speaker-robertdickert.png',
          twitter_handle: 'rdickert',
          youtube: '8OnIlN8DYsQ',
          summary: 'Robert is the newest community engineer at MDG. He is as an expert Meetup captain, with experience from multiple cites and have arranged several recorded co-location Meetups in his past.',
          background: '/bg-meteor.png',
          created: (+new Date)
        },
      ];

      _.each(news_documents, function(doc){
        News.upsert({url_name: doc.url_name}, {$set: doc});
      });
    }
  });

  Meteor.publish('news', function() {
    var confirmed = [
      'Josh-Owens',
      'Ekaterina-Kuznetsova',
      'Arunoda-Susiripala',
      'Carl-Littke',
      'Robert-Dickert',
    ];

    return News.find({url_name: {$in: confirmed}});
  });

  Meteor.publish('article', function(_id) {
    return News.find({_id: _id});
  });
}
