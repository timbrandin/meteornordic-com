sitemaps.add('/sitemap.xml', function() {
  // 'page' is required
  // 'lastmod', 'changefreq', 'priority' are optional
  return [
    { page: '/', lastmod: new Date().getTime(), changefreq: 'daily', priority: 1 },
    { page: '/livestream', lastmod: new Date().getTime(), changefreq: 'daily', priority: 1 },
    { page: '/schedule', lastmod: new Date().getTime(), changefreq: 'daily', priority: 1 },
    { page: '/speakers', lastmod: new Date().getTime(), changefreq: 'daily', priority: 1 },
    { page: '/attendies', lastmod: new Date().getTime(), changefreq: 'daily', priority: 1 },
    { page: '/about', lastmod: new Date().getTime(), changefreq: 'daily', priority: 1 },
    { page: '/speakers/Ekaterina-Kuznetsova', lastmod: new Date().getTime(), changefreq: 'daily', priority: 0.9 },
    { page: '/speakers/Arunoda-Susiripala', lastmod: new Date().getTime(), changefreq: 'daily', priority: 0.9 },
    { page: '/speakers/Carl-Littke', lastmod: new Date().getTime(), changefreq: 'daily', priority: 0.6 },
    { page: '/speakers/Josh-Owens', lastmod: new Date().getTime(), changefreq: 'daily', priority: 0.6 },
    { page: '/speakers/Robert-Dickert', lastmod: new Date().getTime(), changefreq: 'daily', priority: 0.6 },
  ];
});
